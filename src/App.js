import React, { Component } from 'react';
import './App.css';
import List from './components/List.js';


class App extends Component {
  constructor() {
    super()
    this.state = {
      list1: [
        { title: 'Do homework' },
        { title: 'Play game' },
        { title: 'Cooking' },
        { title: 'Get sister' }
      ],
      list2: [],
      list3: [],
      list4: []
    }
  }
  onKeyUp = (event) => {
    if (event.keyCode === 13) {
      if (event.target.value === '' || !event.target.value) {
        return;
      }
      this.setState({
        newItem: '',
        list1: [
          {
            title: event.target.value,
          },
          ...this.state.list1,
        ]
      })
      event.target.value = '';
    }
  }
  handleChange = (fromList, toList, index) => {
    const from = [...this.state[fromList]]
    const to = [...this.state[toList]]
    let x = from[index]
    to.push(x)
    from.splice(index, 1)
    this.setState({
      [fromList]: from,
      [toList]: to
    })
  }

  handleRemove = (fromList, index) => {
    const list =  [...this.state[fromList]]
    list.splice(index, 1)
    this.setState({
      [fromList]:list
    })
  }

  render() {
    return (
      <div className='container'>

        <input placeholder='Add here' type="text"
          id='input' onKeyUp={this.onKeyUp} />
        <div className='title'>
          <p>Problem waiting</p>
          <p>Not for me</p>
          <p>Working</p>
          <p>Done</p> </div>
        <div className='list'>
          <List id='list1' items={this.state.list1} onChange={this.handleChange} onClick={this.handleRemove} />
          <List id='list2' items={this.state.list2} onChange={this.handleChange} onClick={this.handleRemove} />
          <List id='list3' items={this.state.list3} onChange={this.handleChange} onClick={this.handleRemove} />
          <List id='list4' items={this.state.list4} onChange={this.handleChange} onClick={this.handleRemove} />
        </div>
      </div>
    )
  }
}




export default App;
