import React, { Component } from 'react';
import './dragdrop.css';

class List extends Component {

  handleAllow = (e) => {
    e.preventDefault()

  }
  handleDrag = (e) => {
    const item = e.target.closest('[data-item]')
    const index = item.dataset.item
    const list = this.props.id
    e.dataTransfer.setData("item", index);
    e.dataTransfer.setData("list", list);


  }
  handleDrop = (e) => {
    e.preventDefault();
    const index = e.dataTransfer.getData('item')
    const fromList = e.dataTransfer.getData('list')
    const toList = this.props.id
    this.props.onChange(fromList, toList, index)
  }

  onRemove = (e) => {
    const item = e.target.closest('[data-item]')
    const index = item.dataset.item
    const list = this.props.id
    this.props.onClick(list, index)
  }
  
  render() {
    const { items } = this.props;

    return (

      <div className='listItem'
        onDrop={this.handleDrop}
        onDragOver={this.handleAllow}
        onDragStart={this.handleDrag}>
        {items.map((item, index) => {
          return <div draggable key={index} data-item={index}>
            <p> {item.title}</p>

            <button onClick={this.onRemove}
              className='RemoveButton'>X</button>
          </div>

        })}

      </div>


    )
  }
}
export default List;